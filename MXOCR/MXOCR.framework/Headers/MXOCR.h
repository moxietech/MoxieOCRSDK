//
//  MXOCR.h
//  MoxieFaceSDKDemo
//
//  Created by joker on 2018/11/13.
//  Copyright © 2018 scropion. All rights reserved.
//
//  Vsesion 1.3.0
//

#import <Foundation/Foundation.h>
#import "MXOCRResult.h"

/**
 *  OCR识别方向
 */
typedef NS_ENUM(NSInteger, MXOCRSide)
{
    mxIDCardALL = 0      ,   //连续识别正反面
    mxIDCardFrontal      ,   //身份证正面
    mxIDCardBack         ,   //身份证背面
};

/**
 *  出错原因
 */
typedef NS_OPTIONS(NSInteger, MXIDCardErrorCode) {
    // 单图识别失败
    mxIDCardIdentifyFailed = 0,
    // 获取相机权限失败
    mxIDCardCameraAuthorizationFailed = 1,
    // 初始化失败
    mxIDCardHandleInitFailed = 2,
    // 无效参数
    mxIDCardHandleParameterFaild = 3,
    // 句柄错误、内存不足、运存失败、定义缺失、不支持图像格式、
    mxIDCardHandleError = 4,
    // 文件不存在
    mxIDCardFILENOTFOUND = 5,
    // 模型格式不正确
    mxIDCardModelError = 6,
    // 模型文件过期
    mxIDCardModelExpire = 7,
    // license文件不合法
    mxIDCardLicenseError = 8,
    // 包名错误
    mxIDCardAppIDError = 9,
    // SDK过期
    mxIDCardSDKExpire = 10
};

/**
 *  OCR识别代理回调方法
 */
@protocol MXOCRDelegate <NSObject>

// 获取身份证识别结果
- (void)receiveOcrResult:(MXOCRResult *)idcard;
// 连续识别返回结果
- (void)receiveOcrResultWithFront:(MXOCRResult *)front back:(MXOCRResult *)back;
// 识别出错
- (void)receiveOcrError:(MXIDCardErrorCode)errorCode;
//取消识别
- (void)receiveOcrCancel;
//超时自动取消
- (void)receiveOcrTimerOut;

@end


/**
 可设置参数
 */
@interface MXOCRParams : NSObject
@property (nonatomic, assign) MXOCRSide side;//识别方向(支持正面/反面/连续识别)
@property (nonatomic, assign) float autoCancelTime;//识别超时时间(超时后自动退出SDK并回调代理方法 receiveOcrTimerOut)
@property (nonatomic, assign) BOOL showAnimation;//是否展示动画
@property (nonatomic, strong) UILabel* hintLabel;//自定义扫描框上部的提示文本
@end

@interface MXOCR : NSObject


/**
 获取sdk对象

 @return sdk对象
 */
+ (MXOCR *)shared;

/**
  开启OCR功能 不设置side的情况下默认打开正面识别

 @param fromController 传入当前视图控制器
 */
- (void)start:(UIViewController*)fromController params:(MXOCRParams*)params delegate:(id<MXOCRDelegate>)delegate;

/*! @brief finish 程序控制关闭摄像头并取消
 */
- (void)finish; // 允许其他 Controller 主动控制关闭

/**
 获取SDK版本号
 
 @return SDK版本号
 */
+ (NSString*)getVersion;


@end

