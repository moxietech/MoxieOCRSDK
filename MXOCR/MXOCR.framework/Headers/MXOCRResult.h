//
//  MXOCRResult.h
//  MoxieFaceSDKDemo
//
//  Created by joker on 2018/11/13.
//  Copyright © 2018 scropion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MXIDCardSide)
{
    mxIDCardSideFront = 1,  //身份证正面
    mxIDCardSideBack,       //身份证背面
};

//MXOCRResult
@interface MXOCRResult : NSObject

// Image of Card & Face
@property (nonatomic, strong) UIImage    *imgOriginCaptured;     //摄像头捕捉到的图像
@property (nonatomic, strong) UIImage    *imgOriginCroped;       //摄像头捕捉到的框内的图像
@property (nonatomic, strong) UIImage    *imgCardDetected;       //检测出的卡片图像(裁剪图)
@property (nonatomic, strong) UIImage    *imgCardFace;           //检测出的卡片人像

/*! @brief bFaceExist 是否存在人像
 */
@property (nonatomic, assign) BOOL bFaceExist;

@property (nonatomic, assign) MXIDCardSide side;

@end



