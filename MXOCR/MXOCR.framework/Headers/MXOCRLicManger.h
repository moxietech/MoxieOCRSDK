//
//  LFUpLicenseManger.h
//  BankCardScan
//
//  Created by linkface on 2018/9/7.
//  Copyright © 2018年 SenseTime. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MXOCRLicManger : NSObject

+ (void)initWithProductionMode:(BOOL)isProduction;

@end
