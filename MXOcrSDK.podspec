Pod::Spec.new do |s|
  s.name         = "MXOcrSDK" # 项目名称
  s.version      = "1.3.0"        # 版本号 与 你仓库的 标签号 对应
  s.license      = "MIT"          # 开源证书
  s.summary      = "MXOcrSDK for cocoaPods" # 项目简介

  s.homepage     = "https://gitee.com/moxietech/MoxieOCRSDK" # 你的主页
  s.source       = { :git => "https://gitee.com/moxietech/MoxieOCRSDK.git", :tag => "#{s.version}" }#你的仓库地址，不能用SSH地址
  s.source_files  = "MXOCR/*.{a,h,m,framework}" # 你代码的位置
  s.requires_arc = true # 是否启用ARC
  s.platform     = :ios, "7.0" #平台及支持的最低版本
  s.resource     = 'MXOCR/Resource/*'
  #你的SDK路径
  s.vendored_frameworks = 'MXOCR/MXOCR.framework'
  s.public_header_files = 'MXOCR/MXOCR.framework/Headers/*.h'
  s.source_files = 'MXOCR/MXOCR.framework/Headers/*.{h}'
  s.user_target_xcconfig = { 'OTHER_LINK_FLAGS' => '-ObjC', 'ENABLE_BITCODE' => 'NO' }
  s.frameworks   = 'AVFoundation','CoreMedia','ImageIO'
  s.libraries    = 'stdc++'

  s.static_framework = true

  # User
  s.author       = { "moxie" => "dev@51dojo.com" } # 作者信息
  
end